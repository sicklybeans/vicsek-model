use std::time::Instant;
use rand::Rng;
use rand::rngs::ThreadRng;
use rand::distributions::Distribution;
use statrs::distribution::Normal;

use math::*;

use crate::OUTPUT_FORMAT;
use crate::util;

#[derive(Clone)]
/// Placeholder for future extensions.
pub enum BoundaryConditions {
  Periodic,
}

#[derive(Clone)]
/// Placeholder for future extensions.
pub enum AlignmentMethod {
  HardDisk(f64),
  LinearDisk(f64),
  PeriodicDisk(f64, f64),
}

#[derive(Clone)]
pub struct SimulationOptions {
  dt: f64,
  t_end: f64,
  velocity: f64,
  noise_d: f64,
  num_steps: u64,
  size: usize,
  bounds: Vector,
  boundary_conditions: BoundaryConditions,
  alignment_method: AlignmentMethod,
}

pub struct Simulation {
  pub opts: SimulationOptions,
  pub time: f64,
  pub positions: util::UpdateBuffer<Vec<Point>>,
  pub angles: util::UpdateBuffer<Vec<f64>>,
  normal: Normal,
  rng: ThreadRng,
  plot: plotter::Plot,
}

impl Simulation {

  pub fn new(opts: SimulationOptions, plot_dims: (u32, u32)) -> Simulation {
    let mut rng = rand::thread_rng();
    let num = opts.size;
    let positions = (0..num).map(|_| Point::new(rng.gen(), rng.gen())).collect();
    let angles = (0..num).map(|_| rng.gen_range(0.0, TWO_PI)).collect();

    let mut sim = Simulation {
      opts: opts.clone(),
      time: 0.0,
      positions: util::UpdateBuffer::from(positions),
      angles: util::UpdateBuffer::from(angles),
      rng,
      normal: Normal::new(0.0, (2.0 * opts.noise_d * opts.dt).sqrt()).unwrap(),
      plot: plotter::Plot::new(plot_dims.0, plot_dims.1),
    };
    sim.plot.set_plotting_range(0.0..sim.opts.bounds.x, 0.0..sim.opts.bounds.y);
    sim
  }

  pub fn run(mut self, base_dir: &str, verbose: bool) -> String {
    // Prepare output directories
    let output_dir = self.get_output_dir(base_dir);
    util::make_dir(base_dir);
    util::make_dir(&output_dir);

    let t0 = Instant::now();
    let pbar = util::get_progress_bar("Running simulation", self.opts.num_steps, verbose);
    for i in 0..self.opts.num_steps {
      self.time = self.opts.dt * (i as f64);
      self.output(&output_dir, i);
      self.update_position();
      self.update_orientation();
      pbar.inc(1);
    }
    pbar.finish();
    println!("Finished in {} seconds", (t0.elapsed().as_millis() as f64) / 1000.0);
    output_dir
  }

  fn update_orientation(&mut self) {
    match self.opts.alignment_method {
      AlignmentMethod::HardDisk(distance) => {
        for (i, p1) in self.positions.old().iter().enumerate() {
          let neighbors = &self.positions.old().iter()
            .enumerate()
            .filter(|(_, p2)| (p1 - p2).norm() < distance)
            .map(|(j, _)| self.angles.old()[j])
            .collect::<Vec<f64>>();
          let new_angle = mean_angle(&neighbors) + self.normal.sample(&mut self.rng);
          self.angles.new()[i] = new_angle.rem_euclid(TWO_PI); // i.e. mod 2 PI
        }
      },
      AlignmentMethod::PeriodicDisk(mut distance, freq) => {
        distance = if (freq * self.time).cos() > 0.0 {
          distance
        } else {
          0.0
        };
        for (i, p1) in self.positions.old().iter().enumerate() {
          let neighbors = &self.positions.old().iter()
            .enumerate()
            .filter(|(_, p2)| (p1 - p2).norm() <= distance)
            .map(|(j, _)| self.angles.old()[j])
            .collect::<Vec<f64>>();
          let new_angle = mean_angle(&neighbors) + self.normal.sample(&mut self.rng);
          self.angles.new()[i] = new_angle.rem_euclid(TWO_PI); // i.e. mod 2 PI
        }
      },
      AlignmentMethod::LinearDisk(distance) => {
        for (i, p1) in self.positions.old().iter().enumerate() {
          let neighbors = &self.positions.old().iter()
            .enumerate()
            .filter_map(|(j, p2)| {
              let d = (p1 - p2).norm();
              if d < distance {
                Some((j, d))
              } else {
                None
              }
            })
            .map(|(j, d)| {
              let weight = (distance - d) / distance;
              (self.angles.old()[j], weight)
            })
            .collect::<Vec<(f64, f64)>>();
          let new_angle = compute_avg_angle_weighted(&neighbors) + self.normal.sample(&mut self.rng);
          self.angles.new()[i] = new_angle.rem_euclid(TWO_PI); // i.e. mod 2 PI
        }
      }
    }
    self.angles.swap_buffers();
  }

  fn update_position(&mut self) {
    let vdt = self.opts.velocity * self.opts.dt;
    for (i, angle) in self.angles.old().iter().enumerate() {
      let dpos = Vector::new(angle.cos(), angle.sin()) * vdt;
      self.positions.new()[i] = &self.positions.old()[i] + &dpos;

      match self.opts.boundary_conditions {
        BoundaryConditions::Periodic => {
          self.positions.new()[i].bound_periodic(self.opts.bounds.x, self.opts.bounds.y);
        }
      }
    }
    self.positions.swap_buffers();
  }

  fn output(&mut self, output_dir: &str, index: u64) {
    self.plot.clear();
    let line = plotter::Line { color: plotter::BLUE, radius: 6 };
    self.plot.draw_lines(&line, self.positions.old(), self.angles.old());
    self.plot.save(&format!("{}/{:04}.{}", output_dir, index, OUTPUT_FORMAT)).unwrap();
  }

  fn get_output_dir(&self, base_dir: &str) -> String {
    format!("{}/sim-vel{}-D{}-{}", base_dir, self.opts.velocity, self.opts.noise_d, self.opts.alignment_method.name())
  }

}

impl SimulationOptions {
  pub fn new(number: usize, dt: f64, t_end: f64, velocity: f64, rotational_d: f64, alignment_method: AlignmentMethod) -> SimulationOptions {
    SimulationOptions {
      dt,
      t_end,
      velocity,
      noise_d: rotational_d,
      size: number,
      num_steps: (t_end / dt).ceil() as u64,
      bounds: (1.0, 1.0).into(),
      boundary_conditions: BoundaryConditions::Periodic,
      alignment_method,
    }
  }
}

impl AlignmentMethod {
  pub fn name(&self) -> String {
    match self {
      AlignmentMethod::HardDisk(size) => {
        format!("hard-disk{}", size)
      },
      AlignmentMethod::LinearDisk(size) => {
        format!("linear-disk{}", size)
      },
      AlignmentMethod::PeriodicDisk(size, freq) => {
        format!("periodic-disk{}-freq{}", size, freq)
      },
    }
  }
}
