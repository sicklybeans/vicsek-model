use std::borrow::Borrow;
use std::ops::Range;
use image::{ImageError, RgbImage};
use math::*;

use crate::{BLACK, Color, GREY, TwoD, WHITE};
use crate::plot_frame::PlotFrame;
use crate::shapes::{Circle, Line};

pub struct Plot {
  pub width: i64, // more efficient to store as i64 due to `put_pixel_safe`
  pub height: i64, // more efficient to store as i64 due to `put_pixel_safe`
  pub frame: PlotFrame,
  bg_color: Color,
  canvas_color: Color,
  canvas_bounds: TwoD<Range<i64>>,
  frame_color: Option<Color>,
  image: image::RgbImage,
}

impl Plot {

  pub fn new(width: u32, height: u32) -> Plot {
    let mut p = Plot {
      bg_color: WHITE,
      canvas_color: GREY,
      width: width as i64,
      height: height as i64,
      canvas_bounds: (0..(width as i64), 0..(height as i64)).into(),
      frame_color: Some(BLACK),
      frame: PlotFrame::new(
        (0..(width as i64), 0..(height as i64)),
        (0.0..1.0, 0.0..1.0)),
      image: RgbImage::new(width, height),
    };
    p.set_drawing_bounds(0.05..0.95, 0.05..0.95);
    p
  }

  pub fn clear(&mut self) {
    let x_bounds = self.canvas_bounds.x.clone();
    let y_bounds = self.canvas_bounds.y.clone();
    for y in 0..self.height {
      for x in 0..self.width {
        if x_bounds.contains(&x) && y_bounds.contains(&y) {
          self.image.put_pixel(x as u32, y as u32, self.canvas_color);
        } else {
          self.image.put_pixel(x as u32, y as u32, self.bg_color);
        }
      }
    }
    if let Some(color) = self.frame_color {
      for y in y_bounds.clone() {
        self.image.put_pixel(x_bounds.start as u32, y as u32, color);
        self.image.put_pixel(x_bounds.end as u32, y as u32, color);
      }
      for x in x_bounds.clone() {
        self.image.put_pixel(x as u32, y_bounds.start as u32, color);
        self.image.put_pixel(x as u32, y_bounds.end as u32, color);
      }
    }
  }

  pub fn draw_lines<T>(&mut self, line: &Line, positions: &[T], angles: &[f64])
  where T: Borrow<Point> {
    for i in 0..positions.len() {
      line.draw(self, *positions[i].borrow(), angles[i]);
    }
  }

  pub fn draw_circles<T>(&mut self, circle: &Circle, positions: &[T])
  where T: Borrow<Point> {
    for i in 0..positions.len() {
      circle.draw(self, *positions[i].borrow());
    }
  }

  pub fn save(&self, filename: &str) -> Result<(), ImageError> {
    self.image.save(filename)
  }

  pub fn set_drawing_bounds<R: Into<Range<f64>>>(&mut self, x_bounds: R, y_bounds: R) {
    let w = self.width as f64;
    let h = self.height as f64;
    let x = x_bounds.into();
    let y = y_bounds.into();
    self.canvas_bounds.x = ((x.start * w + 0.5) as i64)..((x.end * w + 0.5) as i64);
    self.canvas_bounds.y = ((y.start * h + 0.5) as i64)..((y.end * h + 0.5) as i64);
    self.frame.set_drawing_bounds(self.canvas_bounds.x.clone(), self.canvas_bounds.y.clone());
  }

  pub fn set_plotting_range<R: Into<Range<f64>>>(&mut self, x_range: R, y_range: R) {
    self.frame.set_plotting_range(x_range, y_range);
  }

  pub fn put_pixel_safe(&mut self, x: i64, y: i64, c: Color) {
    if self.canvas_bounds.x.contains(&x) && self.canvas_bounds.y.contains(&y) {
      self.image.put_pixel(x as u32, y as u32, c)
    }
  }

  pub fn with_bg_color(mut self, color: Color) -> Self {
    self.bg_color = color;
    self
  }

  pub fn with_canvas_color(mut self, color: Color) -> Self {
    self.canvas_color = color;
    self
  }

  pub fn with_frame_color(mut self, color: Option<Color>) -> Self {
    self.frame_color = color;
    self
  }

}
